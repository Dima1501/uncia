$(document).ready(function () {

	headerScroll();

	pAbout();

	pInfo();

	validateForm();

	headerNav();

	anchors();
})

const anchors = () => {

	$('[data-anchor]').on('click', function () {

		let name = $(this).attr('data-anchor');

		let offset = $('#' + name).offset().top - 90

		$("html, body").stop().animate({scrollTop:offset}, 500, 'swing');

		$('.header').removeClass('header--opened')

		return false
	})
}

const headerScroll = () => {

	$(window).on('scroll', function () {

		let scrolled = $(window).scrollTop();

		if (scrolled > 10) {
			$('.header').addClass('header--scrolled')
		} else {
			$('.header').removeClass('header--scrolled')
		}
	})
}

const pAbout = () => {

	$('.p-about__item-more').on('click', function () {

		$(this).toggleClass('is-opened')

		$(this).next().slideToggle();
	})
}

const pInfo = () => {

	$('.p-info__more').on('click', function () {

		$(this).toggleClass('is-opened')

		$(this).prev('.p-info__hidden').slideToggle()
	})
}

const validateForm = () => {

	if ($('.js-validate').length > 0) {

		$('.js-validate').each(function () {

			$(this).validate({
				rules: {
					card: "required"
				},

				messages: {
					card: "Введите Номер Карты"
				},

				submitHandler: function () {

					$('.c-results').fadeIn()
					
				}
			});
		});
	}
}

const headerNav = () => {

	$('.burger').on('click', function() {
		$('.header').toggleClass('header--opened')
	})
}














